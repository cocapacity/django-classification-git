import os

from setuptools import setup, find_packages

def read(fname):
    return open(os.path.join(os.path.dirname(__file__), fname)).read()


install_requires = [
    'south',
    'django-mptt'
],

setup(
    name = "django-classification",
    version="13.01",
    description = "@TODO",
    long_description='\n\n'.join([read("README.rst"), read("CHANGES.rst")]),
    url = '@TODO',
    license = 'BSD',
    author = 'Co-Capacity',
    author_email = 'django@co-capacity.org',
    packages = find_packages(),
    include_package_data=True,
    install_requires = install_requires,
    zip_safe=False,

    classifiers = [
        'Development Status :: 4 - Beta',
        'Environment :: Web Environment',
        'Framework :: Django',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: BSD License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Topic :: Internet :: WWW/HTTP',
    ]
)
