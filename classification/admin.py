from django.contrib.admin import ModelAdmin


class BaseClassificationAdmin(ModelAdmin):
    list_display = ('name',)
    search_fields = ('name',)
    prepopulated_fields = {'slug': ('name',)}
