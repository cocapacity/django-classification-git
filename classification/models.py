from django.db import models
from django.utils.encoding import force_unicode
from django.template.defaultfilters import slugify
from django.utils.translation import ugettext_lazy as _

from mptt.models import MPTTModel
from mptt.fields import TreeForeignKey
from mptt.managers import TreeManager


class BaseClassification(MPTTModel):
    """
    This base classification model.
    """
    parent = TreeForeignKey('self',
                            blank=True,
                            null=True,
                            related_name="children",
                            verbose_name=_('Parent'))
    name = models.CharField(_('Name'), max_length=255)
    slug = models.SlugField(max_length=255, blank=True)

    class Meta:
        abstract = True
        unique_together = ('parent', 'name')
        # correct order if somebody overrides default manager
        ordering = ('tree_id', 'lft')

    class MPTTMeta:
        # we are mostly static, so extra query on insert not a problem.
        order_insertion_by = 'name'

    def __unicode__(self):
        # be carefull this gives n + 1 queries when looping, use mppt.forms!
        ancestors = self.get_ancestors(include_self=True)
        return u' > '.join([force_unicode(c.name) for c in ancestors])

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(self.name)

        super(BaseClassification, self).save(*args, **kwargs)
