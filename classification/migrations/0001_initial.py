# -*- coding: utf-8 -*-
from south.v2 import SchemaMigration


class Migration(SchemaMigration):
    """
    This is just here so other apps can depend on it.
    """

    def forwards(self, orm):
        pass

    def backwards(self, orm):
        pass

    models = {}

    complete_apps = ['classification']